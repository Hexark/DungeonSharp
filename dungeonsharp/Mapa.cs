﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dungeonsharp
{
    [Serializable]
    class Mapa
    {
        private int idmapa;
        private string nombremapa;
        private string descripcionmapa;
        private Boolean bloqueada;
        private int norte;
        private int este;
        private int sur;
        private int oeste;

        public Mapa(int idmapa, string nombremapa, string descripcion, bool bloqueada, int norte, int este, int sur, int oeste)
        {
            this.idmapa = idmapa;
            this.nombremapa = nombremapa;
            this.descripcionmapa = descripcion;
            this.bloqueada = bloqueada;
            this.norte = norte;
            this.este = este;
            this.sur = sur;
            this.oeste = oeste;
        }
        
        //getters

        public string getNombreMapa(){
            return this.nombremapa;  
        }
        public string getDescripcionMapa()
        {
            return this.descripcionmapa;
        }
        public Boolean getBloqueada()
        {
            return this.bloqueada;
        }
        public int getDirecciones(string opcion)
        {
            switch (opcion)
            {
                case "n":
                    return this.norte;
                case "e":
                    return this.este;
                case "s":
                    return this.sur;
                case "o":
                    return this.oeste;
                case "a":
                    return this.norte + this.este + this.sur + this.oeste;
                default:
                    return 1;
            }
        }

        public int getIdMapa()
        {
            return this.idmapa;
        }

        //setters

        public void setBloqueada(Boolean bloqueado)
        {
            this.bloqueada = bloqueado;
        }
    }
}
