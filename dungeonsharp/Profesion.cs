﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dungeonsharp
{
    [Serializable]
    class Profesion
    {
        int idprofesion;
        string nombreprofesion;
        string caracteristicas;

        public Profesion(int id , string profesion, string caracteristicas)
        {
            this.idprofesion = id;
            this.nombreprofesion = profesion;
            this.caracteristicas = caracteristicas;
        }



        public string getNombreProfesion()
        {
            return this.nombreprofesion;
        }
        public string getCaracteristicas()
        {
            return this.caracteristicas;
        }
        public int getIdProfesion()
        {
            return this.idprofesion;
        }

    }
}
