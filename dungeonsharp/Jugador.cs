﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace dungeonsharp
{
    [Serializable]
    class Jugador : Personaje
    {

        public Jugador()
        {
            Console.Clear();
            base.idpersonaje = 1;
            Console.WriteLine("Dime tu nombre: ");
            string nombre = Console.ReadLine();
            base.nombrepersonaje = nombre;
            
            Console.WriteLine("¿Que profesion quieres elegir?: ");

            foreach (var item in Program.listaprofesiones.Keys) 
            { 
                Console.WriteLine("\n" +Program.listaprofesiones[item].getIdProfesion() + " " + Program.listaprofesiones[item].getNombreProfesion() +  " " + Program.listaprofesiones[item].getCaracteristicas());
            }
            string opcion = Console.ReadLine();
            switch (opcion)
            {
                case "1":
                    this.subirEstadisticas(50, 0, 0.0f, 10, 0);
                    this.setProfesion(Program.listaprofesiones[1]);
                    break;
                case "2":
                    this.subirEstadisticas(-20, 50, 0.0f, 10, 0);
                    this.setProfesion(Program.listaprofesiones[2]);
                    break;
                case "3":
                    this.subirEstadisticas(-40, 0, 0.0f, 25, 0);
                    this.setProfesion(Program.listaprofesiones[3]);
                    break;
                default:
                    Console.WriteLine("Introduce una opción válida");
                    break;
            }
            Console.WriteLine(this.mostrarEstadisticas());
            Console.ReadLine();
        }
        public void setJugadorMapa(Mapa m)
        {
            this.mapa = m;
        }
        public string posicionJugador()
        {
            return this.mapa.getNombreMapa() + "\n" + this.mapa.getDescripcionMapa();
        }
        public int getIdPosicionMapa()
        {
            return this.mapa.getIdMapa();
        }
        
        public void mover(string direccion)
        {
            int mapanuevo;
            switch (direccion)
            {
                case "n":
                    mapanuevo = this.mapa.getDirecciones("n");
                    Console.Clear();
                    if (mapanuevo != 0)
                        if (!Program.listamapas[mapanuevo].getBloqueada())
                        {
                            this.mapa = Program.listamapas[mapanuevo];
                            Console.Clear();
                        }
                        else
                        {
                            Console.Clear();
                            Console.WriteLine("Está bloqueada");
                            Console.ReadLine();
                        }
                    else
                        {
                            Console.Clear();
                            Console.WriteLine("No hay ruta por ese sitio");
                            Console.ReadLine();
                        }
                    break;
                case "e":
                    mapanuevo = this.mapa.getDirecciones("e");
                     Console.Clear();
                    if (mapanuevo != 0)
                        if (!Program.listamapas[mapanuevo].getBloqueada())
                        {
                            this.mapa = Program.listamapas[mapanuevo];
                            Console.Clear();
                        }
                        else
                        {
                            Console.Clear();
                            Console.WriteLine("Está bloqueada");
                            Console.ReadLine();
                        }
                    else
                        {
                            Console.Clear();
                            Console.WriteLine("No hay ruta por ese sitio");
                            Console.ReadLine();
                        }
                    break;
                case "s":
                    mapanuevo = this.mapa.getDirecciones("s");
                    Console.Clear();
                    if (mapanuevo != 0)
                        if (!Program.listamapas[mapanuevo].getBloqueada())
                        {
                            this.mapa = Program.listamapas[mapanuevo];
                            Console.Clear();
                        }
                        else
                        {
                            Console.Clear();
                            Console.WriteLine("Está bloqueada");
                            Console.ReadLine();
                        }
                    else
                        {
                            Console.Clear();
                            Console.WriteLine("No hay ruta por ese sitio");
                            Console.ReadLine();
                        }
                    break;
                case "o":
                    mapanuevo = this.mapa.getDirecciones("o");
                    Console.Clear();
                    if (mapanuevo != 0)
                        if (!Program.listamapas[mapanuevo].getBloqueada())
                        {
                            this.mapa = Program.listamapas[mapanuevo];
                            Console.Clear();
                        }
                        else
                        {
                            Console.Clear();
                            Console.WriteLine("Está bloqueada");
                            Console.ReadLine();
                        }
                    else
                        {
                            Console.Clear();
                            Console.WriteLine("No hay ruta por ese sitio");
                            Console.ReadLine();
                        }
                    break;
                default:
                    Console.WriteLine("Introduce una opción valida");
                    break;
            }
         

        }

        public void setEstadisticas(){

        }

        //Se puede usar para subir y bajar estadisticas, para bajar estaditicas introduces un numero negativo ej: -40
        public void subirEstadisticas(int salud, int mana, float indicecritico, int ataque, int armadura){
            this.salud = this.salud + salud;
            this.mana = this.mana + mana;
            this.indicecritico = this.indicecritico + indicecritico;
            this.ataque = this.ataque + ataque;
            this.armadura = this.armadura + armadura;
        }

        public void setProfesion(Profesion profesion )
        {
            this.profesion = profesion;
        }

        public void atacar(string nombreenemigo)
        {
                Random r = new Random();
                foreach (var item in Program.listaenemigos.Keys)
                {
                    if (Program.listaenemigos[item].getNombrePersonaje() == nombreenemigo)
                    {
                        while (this.getSalud() > 0 && Program.listaenemigos[item].getSalud() > 0)
                        {
                            int numerojugador = r.Next(0, 10);
                            int numeroenemigo = r.Next(0, 10);
                            if (numerojugador > numeroenemigo)
                            {
                                int saludenemigo = Program.listaenemigos[item].getSalud() - this.ataque;
                                Console.WriteLine("Has atacado a " + Program.listaenemigos[item].getNombrePersonaje() + " y le has inflingido " + this.ataque + " y le queda " + saludenemigo + " de salud");
                                Thread.Sleep(500);
                                Program.listaenemigos[item].setSalud(saludenemigo);
                            }
                            else
                            {
                                int saludjugador = this.salud - Program.listaenemigos[item].getAtaque();
                                Console.WriteLine("El enemigo " + Program.listaenemigos[item].getNombrePersonaje() + " te ha atacado y te ha inflingido " + Program.listaenemigos[item].getAtaque() + " y te queda " + saludjugador);
                                Thread.Sleep(500);
                                this.salud = saludjugador;
                            }

                        }
                        if (Program.listaenemigos[item].getSalud() < 0)
                        {
                            Console.WriteLine("El enemigo " + Program.listaenemigos[item].getNombrePersonaje() + " ha muerto");
                            Console.WriteLine("Has ganado " + Program.listaenemigos[item].getExperiencia() + " de experiencia");
                            this.experiencia = this.experiencia + Program.listaenemigos[item].getExperiencia();
                            Program.listaenemigos.Remove(item);
                            
                        }
                        if (this.salud < 0)
                        {
                            Console.WriteLine("Has muerto");
                            
                        }
                        break;
                    }

                }
            
             Console.ReadLine();
            
        }
    }
}
