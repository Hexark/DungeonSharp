﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dungeonsharp
{
    [Serializable]
    class Personaje
    {
        //Atributos

        protected int idpersonaje;
        protected string nombrepersonaje;
        protected int nivel;
        protected Profesion profesion;
        protected int experiencia;
        protected Mapa mapa;
        //Estadisticas
        protected int salud;
        protected int mana;
        protected float indicecritico;
        protected int ataque;
        protected int armadura;

        public Personaje()
        {
            nombrepersonaje = "";
            salud = 100;
            mana = 0;
            ataque = 1;
            armadura = 0;
            experiencia = 0;
            nivel = 1;

        }

        public int getIdPersonaje()
        {
            return this.idpersonaje;
        }
        public string getNombrePersonaje()
        {
            return this.nombrepersonaje;
        }
        public int getNivel()
        {
            return this.nivel;
        }
        public int getSalud()
        {
            return this.salud;
        }
        public int getMana()
        {
            return this.mana;
        }
        public float getIndiceCritico()
        {
            return this.indicecritico;
        }
        public int getAtaque()
        {
            return this.ataque;
        }
        public int getArmadura()
        {
            return this.armadura;
        }
        public int getExperiencia()
        {
            return this.experiencia;
        }
        public string getProfesion()
        {
            return this.profesion.getNombreProfesion();
        }
        public string mostrarEstadisticas()
        {
            return "Nombre del personaje: " + getNombrePersonaje() + "\nProfesion: " + this.profesion.getNombreProfesion() +"\nNivel del personaje: " + getNivel() + "\nSalud del personaje: " + getSalud() + "\nMana del personaje: " + getMana() + "\nAtaque del personaje: " + getAtaque() + "\nIndice critico del personaje: " + getIndiceCritico() + "\nExperiencia: " + getExperiencia() + "/100"; 
        }
        public void setNombre(string nombre)
        {
            this.nombrepersonaje = nombre;
        }
        public void setIdPersonaje(int id)
        {
            this.idpersonaje = id;
        }

    }
}
