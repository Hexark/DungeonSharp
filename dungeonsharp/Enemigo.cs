﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dungeonsharp
{
    [Serializable]
    class Enemigo : Personaje
    {
        public Enemigo(int idenemigo, string nombrenemigo,Mapa mapa, int nivel ,int experiencia, int salud, int ataque)
        {
            this.idpersonaje = idenemigo;
            this.nombrepersonaje = nombrenemigo;
            this.mapa = mapa;
            this.nivel = nivel;
            this.experiencia = experiencia;
            this.salud = salud;
            this.ataque = ataque;
        }

        public int getIdMapa()
        {
            return this.mapa.getIdMapa();
        }
        public void setSalud(int salud)
        {
            this.salud = salud;
        }
    }
}
