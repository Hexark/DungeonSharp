﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;


namespace dungeonsharp
{
    class Partida : Program
    {
        static public void empezarNuevaPartida(Jugador jugador)
        {
            Mapa m1 = new Mapa(1, "Entrada misteriosa", "Estás en el principio de tu aventura", false, 0, 2, 5, 0);
            Mapa m2 = new Mapa(2, "Pasillo escabroso", "Ves como la estructura de la mansión no se aguanta", false, 0, 3, 6, 1);
            Mapa m3 = new Mapa(3, "Habitación del piano", "Has subido una escalera de caracol y te has encontrado con una habitación bastante siniestra en ella encuentras un piano", false, 0, 4, 7, 2);
            Mapa m4 = new Mapa(4, "Cocina", "Ves los fogones de la cocína como aun echan humo.", false, 0, 0, 8, 3);
            Mapa m5 = new Mapa(5, "Biblioteca", "Apenas ves nada de tantas estanterias y libros", false, 1, 6, 0, 0);
            Mapa m6 = new Mapa(6, "Habitación secreta", "por uno de los huecos del pasillo puedes acceder a una habitación muy oscura", false, 0, 7, 0, 0);
            Mapa m7 = new Mapa(7, "Sala de trofeos", "Quien este detrás de todo esto es un sanguinario...", false, 3, 8, 0, 6);
            Mapa m8 = new Mapa(8, "Dormitorio", "Este dormitorio... puedo sentir una presencia muy demoníaca", false, 4, 0, 0, 7);


            Enemigo en1 = new Enemigo(1, "Araña", m1, 1, 3, 15, 2);
            Enemigo en2 = new Enemigo(2, "Esqueleto", m1, 10, 1, 20, 5);

            listamapas.Add(1, m1);
            listamapas.Add(2, m2);
            listamapas.Add(3, m3);
            listamapas.Add(4, m4);
            listamapas.Add(5, m5);
            listamapas.Add(6, m6);
            listamapas.Add(7, m7);
            listamapas.Add(8, m8);

            listaenemigos.Add(1,en1);
            listaenemigos.Add(2, en2);

            jugador.setJugadorMapa(listamapas[1]);
        }
        static public void empezarPartida()
        {
            if(listaenemigos == null){
                Mapa m1 = new Mapa(1, "Entrada misteriosa", "Estás en el principio de tu aventura", false, 0, 2, 5, 0);
                Mapa m2 = new Mapa(2, "Pasillo escabroso", "Ves como la estructura de la mansión no se aguanta", false, 0, 3, 6, 1);
                Mapa m3 = new Mapa(3, "Habitación del piano", "Has subido una escalera de caracol y te has encontrado con una habitación bastante siniestra en ella encuentras un piano", false, 0, 4, 7, 2);
                Mapa m4 = new Mapa(4, "Cocina", "Ves los fogones de la cocína como aun echan humo.", false, 0, 0, 8, 3);
                Mapa m5 = new Mapa(5, "Biblioteca", "Apenas ves nada de tantas estanterias y libros", false, 1, 6, 0 , 0);
                Mapa m6 = new Mapa(6, "Habitación secreta", "por uno de los huecos del pasillo puedes acceder a una habitación muy oscura", false, 0, 7, 0, 0);
                Mapa m7 = new Mapa(7, "Sala de trofeos", "Quien este detrás de todo esto es un sanguinario...", false, 3, 8, 0, 6);
                Mapa m8 = new Mapa(8, "Dormitorio", "Este dormitorio... puedo sentir una presencia muy demoníaca", false, 4, 0, 0, 7);

                Enemigo en1 = new Enemigo(1, "Araña", m1, 1, 3, 15, 2);
                Enemigo en2 = new Enemigo(2, "Esqueleto", m1, 10, 1, 20, 5);

                listamapas.Add(1, m1);
                listamapas.Add(2, m2);
                listamapas.Add(3, m3);
                listamapas.Add(4, m4);
                listamapas.Add(5, m5);
                listamapas.Add(6, m6);
                listamapas.Add(7, m7);
                listamapas.Add(8, m8);


            
                listaenemigos.Add(1, en1);
                listaenemigos.Add(2, en2);
            }
        }
        static public void rutas(Jugador j1)
        {
           // Console.Clear();
            Console.WriteLine("Estas en: " + j1.posicionJugador() + "\nLas posibles rutas son:\n");
            foreach (var item in listamapas.Keys)
            {
                if (item == j1.getIdPosicionMapa())
                {
                    if (listamapas[item].getDirecciones("n") != 0)
                        Console.Write(" (N)orte");
                    if (listamapas[item].getDirecciones("e") != 0)
                        Console.Write(" (E)ste");
                    if (listamapas[item].getDirecciones("s") != 0)
                        Console.Write(" (S)ur");
                    if (listamapas[item].getDirecciones("o") != 0)
                        Console.Write(" (O)ste");
                    Console.WriteLine("\n");
                    break;
                }
            }
            Console.WriteLine("Estos son los enemigos de este mapa: ");
            foreach(var item in listaenemigos.Keys){
                if (listaenemigos[item].getIdMapa() == j1.getIdPosicionMapa()) {
                    Console.WriteLine(listaenemigos[item].getNombrePersonaje());
                }
            }
        }
        static public void prompt(Jugador j1)
        {
                while (true)
                {
                    Console.Clear();
                    rutas(j1);
                    Console.WriteLine("\nPara obtener ayuda , escribe ?");
                    string opcion = Console.ReadLine();
                    switch (opcion.ToLower())
                    {
                        case "n" :
                            j1.mover("n");
                            break;
                        case "e":
                            j1.mover("e");
                            break;
                        case "s":
                            j1.mover("s");
                            break;
                        case "o":
                            j1.mover("o");
                            break;
                        case "salir":
                            Partida.salir();
                            break;
                        case "guardar":
                            Partida.guardarPartida(j1, Program.listaenemigos);
                            Console.WriteLine("Se ha guardado con exito");

                            Console.ReadLine();
                            break;
                        case "estadisticas":
                            Console.WriteLine(j1.mostrarEstadisticas());
                            Console.ReadLine();
                            break;
                        case "cargar":
                            Console.WriteLine("¿Quieres descargar una partida de tu cuenta?(S)i (N)o");
                            string opcion3 = Console.ReadLine().ToLower();
                            if (opcion3 == "s")
                            {
                                Console.WriteLine("Introduce tu usuario:");
                                string usuario = Console.ReadLine();
                                string contrasenya = "";
                                Console.WriteLine("Introduce tu contraseña:");
                                ConsoleKeyInfo keyInfo;
                                do
                                {
                                    keyInfo = Console.ReadKey(true);
                                    // Skip if Backspace or Enter is Pressed
                                    if (keyInfo.Key != ConsoleKey.Backspace && keyInfo.Key != ConsoleKey.Enter)
                                    {
                                        contrasenya += keyInfo.KeyChar;
                                        Console.Write("*");
                                    }
                                    else
                                    {
                                        if (keyInfo.Key == ConsoleKey.Backspace && contrasenya.Length > 0)
                                        {
                                            // Remove last charcter if Backspace is Pressed
                                            contrasenya = contrasenya.Substring(0, (contrasenya.Length - 1));
                                            Console.Write("\b \b");
                                        }
                                    }
                                }
                                // Stops Getting Password Once Enter is Pressed
                                while (keyInfo.Key != ConsoleKey.Enter);
                                FtpCon.leer(usuario,contrasenya);
                                Console.WriteLine("\nIntroduce el nombre de la partida que quieres descargar");
                                string partida = Console.ReadLine();

                                FtpCon.download(partida, "saves/" + partida, usuario, contrasenya);

                            }
                        try
                            {
                                Guardado partida1 = cargarPartida();
                                j1 = partida1.getJugador();
                                Program.listaenemigos = partida1.getListaEnemigos();
                                Console.WriteLine("Has cargado la partida de: " + j1.getNombrePersonaje() + " pulsa la tecla Enter para confirmar");
                                Console.ReadLine();
                            }
                            catch(NullReferenceException)
                            {
                                break;
                            }
                            break;
                        case "?":
                            Console.WriteLine("\n--------------------------------------------\nEstos son los siguientes comandos:\nN-E-S-O:Direcciones a las que moverse.\nGuardar:Permite guardar la partida,\nCargar:Permite cargar la partida.\nAtacar:Atacas al enemigo.\nSalir:Para salir del juego.\nPosicion: muestra la posicion del jugador.\nEstadisticas: muestra las estadisticas del personaje.\n--------------------------------------------\n");
                            Console.ReadLine();
                            break;
                        case "posicion":
                            Console.WriteLine(j1.posicionJugador());
                            break;
                        case "atacar":
                        if (listaenemigos.Count() != 0)
                        {
                            Console.WriteLine("Introduce el nombre del enemigo a atacar: ");
                            string nombreenemigo = Console.ReadLine();
                            j1.atacar(nombreenemigo);
                        }
                        else
                        {
                            Console.WriteLine("No hay ningun enemigo");
                            Console.ReadLine();
                        }
                            break;
                        default:
                            Console.WriteLine("Introduce una opcion valida");
                            Console.ReadLine();
                            break;
                    }
                }
        }

        static public void salir()
        {
            Console.WriteLine("\nHasta Luego!");
            Console.Read();
            Environment.Exit(0);
        }
    }
}
