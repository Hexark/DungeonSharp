﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
namespace dungeonsharp
{
    class Program
    {
        static public void guardarPartida(Jugador j, Dictionary<int,Enemigo> lista)
        {
            Guardado partida = new Guardado(j, lista);
            IFormatter formatter = new  BinaryFormatter();
            FileStream fs = new FileStream("saves\\" + j.getNombrePersonaje().ToLower() + ".sav", FileMode.Create);
            formatter.Serialize(fs, partida);
            fs.Close();
        }
        static public Guardado cargarPartida()
        {
            DirectoryInfo directory = new DirectoryInfo("saves");
            FileInfo[] files = directory.GetFiles("*.sav");
            for (int i = 0; i < files.Length; i++)
            {
                Console.WriteLine("\n" + files[i].Name + "\n");
            }
            Console.WriteLine("Introduce el nombre del fichero que quieras cargar.\nej: partida.sav");
            string pcadena = Console.ReadLine();
            IFormatter formatter = new BinaryFormatter();
            try
            {
                FileStream fs = new FileStream("saves\\" + pcadena, FileMode.Open);
                Guardado partida = formatter.Deserialize(fs) as Guardado;
                fs.Close();
                return partida;
            }
            catch (DirectoryNotFoundException e)
            {
                Console.WriteLine("Ha habido un error: " + e.Message);
                Console.ReadLine();
                return null;
            }
            catch (SerializationException)
            {
                Console.WriteLine("La partida que has intentado cargar está corrupta o vácia");
                Console.ReadLine();
                return null;
            }
        }
        static public Dictionary<int, Mapa> listamapas = new Dictionary<int, Mapa>();
        static public Dictionary<int, Profesion> listaprofesiones = new Dictionary<int, Profesion>();
        static public Dictionary<int, Enemigo> listaenemigos = new Dictionary<int, Enemigo>(); 
        static void Main(string[] args)
        {
            Profesion prof1 = new Profesion(1, "Guerrero","\n -Aumento en 50 la vida\n -Aumento en 10 el ataque\n");
            Profesion prof2 = new Profesion(2, "Mago","\n -Disminución en 20 de vida\n -Aumento en 10 el ataque\n -Aumento del mana en 50\n");
            Profesion prof3 = new Profesion(3, "Asesino","\n -Disminución en 40 de vida\n -Aumento en 25 el ataque");

            listaprofesiones.Add(1, prof1);
            listaprofesiones.Add(2, prof2);
            listaprofesiones.Add(3, prof3);

            string opcion;
            while (true)
            {
                Console.Clear();
                Console.WriteLine("1-Empezar Nueva Partida\n2-Cargar Partida\n3-Salir");
                opcion = Console.ReadLine();
                switch (opcion)
                {

                    case "1":
                        Jugador j1 = new Jugador();
                        Partida.empezarNuevaPartida(j1);
                        Partida.prompt(j1);
                        break;
                    case "2":
                        Console.WriteLine("¿Quieres descargar una partida de tu cuenta?(S)i (N)o");
                        string opcion3 = Console.ReadLine().ToLower();
                        if (opcion3 == "s")
                        {
                            Console.WriteLine("Introduce tu usuario:");
                            string usuario = Console.ReadLine();
                            string contrasenya = "";
                            Console.WriteLine("Introduce tu contraseña:");
                            ConsoleKeyInfo keyInfo;
                            do
                            {
                                keyInfo = Console.ReadKey(true);
                                // Skip if Backspace or Enter is Pressed
                                if (keyInfo.Key != ConsoleKey.Backspace && keyInfo.Key != ConsoleKey.Enter)
                                {
                                    contrasenya += keyInfo.KeyChar;
                                    Console.Write("*");
                                }
                                else
                                {
                                    if (keyInfo.Key == ConsoleKey.Backspace && contrasenya.Length > 0)
                                    {
                                        // Remove last charcter if Backspace is Pressed
                                        contrasenya = contrasenya.Substring(0, (contrasenya.Length - 1));
                                        Console.Write("\b \b");
                                    }
                                }
                            }
                            // Stops Getting Password Once Enter is Pressed
                            while (keyInfo.Key != ConsoleKey.Enter);
                            FtpCon.leer(usuario, contrasenya);
                            Console.WriteLine("\nIntroduce el nombre de la partida que quieres descargar");
                            string partida = Console.ReadLine();

                            FtpCon.download(partida, "saves/" + partida, usuario, contrasenya);

                        }
                        try
                        {
                            Guardado partida1 = cargarPartida();
                            j1 = partida1.getJugador();
                            Program.listaenemigos = partida1.getListaEnemigos();
                            Console.WriteLine("Has cargado la partida de: " + j1.getNombrePersonaje() + " pulsa la tecla Enter para confirmar");
                            Console.ReadLine();
                            Partida.prompt(j1);
                        }
                        catch (NullReferenceException)
                        {
                            break;
                        }
                        break;
                    case "3":
                        Partida.salir();
                        break;
                    default:
                        Console.WriteLine("Introduce una opción válida");
                        Console.ReadLine();
                        break;
                }
                
                
            }
        }
    }
}
