﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dungeonsharp
{
    [Serializable]
    class Guardado
    {
        Jugador j;
        Dictionary<int, Enemigo> l;

        public Jugador getJugador()
        {
            return j;
        }
        public Dictionary<int,Enemigo> getListaEnemigos()
        {
            return l;
        }
    public Guardado(Jugador j, Dictionary<int, Enemigo> l){
        this.j = j;
        this.l = l;
    }
}
}
